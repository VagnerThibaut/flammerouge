public abstract class Cycliste {
	/**
	 * position du cyliste sur le circuit
	 */
	private int positionActuelle;
	/**
	 * � vrai si le cycliste est sur la file droite de la route
	 */
	private boolean fileDroite;
	/**
	 * num�ro de l'�quipe � laquelle appartient le cycliste
	 */
	protected int numequipe;

	/**
	 * @param posActu,
	 *            position du cyliste sur le circuit
	 * @param fileD,
	 *            � vrai si le cycliste est sur la file droite de la route
	 * @param numeq,
	 *            num�ro de l'�quipe � laquelle appartient le cycliste
	 */
	public Cycliste(int posActu, boolean fileD, int numeq) {
		positionActuelle = posActu;
		fileDroite = fileD;
		numequipe = numeq;
	}

	public int getNumEquipe() {
		return numequipe;
	}

	/**
	 * @return les 4 cartes que le joueur � piocher
	 */
	public abstract Cartes[] piocherCartes();

	/**
	 * @return entier repr�sentant la valeur de la carte choisie
	 */
	public abstract int afficherChoisirCartes();

	public abstract String toString();

	/**
	 * methode permettant d'ajouter une carte fatigue dans la defausse
	 */
	public abstract void ajoutCarteFatigue();

	public int getPositionActuelle() {
		return positionActuelle;
	}

	public void setPositionActuelle(int positionActuelle) {
		this.positionActuelle = positionActuelle;
	}

	public boolean isFileDroite() {
		return fileDroite;
	}

	/**
	 * @param fileDroite
	 */
	public void setFileDroite(boolean fileDroite) {
		this.fileDroite = fileDroite;
	}

}
