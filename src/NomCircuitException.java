@SuppressWarnings("serial")
public class NomCircuitException extends Exception {
	public NomCircuitException() {
		System.out.println("Erreur : nom de circuit incorrect (CB, CS, CM, CD).");
	}
}
