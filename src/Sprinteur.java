import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Sprinteur extends Cycliste {
	/**
	 * arraylist de carte qui contient les cartes jouees
	 */
	private ArrayList<Cartes> defausseSprinteur;
	/**
	 * pioche du sprinteur
	 */
	private Pioche pSprinteur;

	/**
	 * Constructeur
	 * 
	 * @param pos,
	 *            position du sprinteur au d�but
	 * @param fileDr,
	 *            � vrai si le sprinteur est sur la file de droite
	 * @param numeq,
	 *            num�ro de l'�quipe du sprinteur
	 */
	public Sprinteur(int pos, boolean fileDr, int numeq) {
		super(pos, fileDr, numeq);
		pSprinteur = new Pioche(1);
		defausseSprinteur = new ArrayList<Cartes>();
	}

	/**
	 * @return la defausse du sprinteur
	 */
	public ArrayList<Cartes> getDefausseSprinteur() {
		return defausseSprinteur;
	}

	/**
	 * @param defausseSprinteur
	 */
	public void setDefausseSprinteur(ArrayList<Cartes> defausseSprinteur) {
		this.defausseSprinteur = defausseSprinteur;
	}

	/**
	 * @return la pioche du sprinteur
	 */
	public Pioche getpSprinteur() {
		return pSprinteur;
	}

	/**
	 * @param pSprinteur
	 */
	public void setpSprinteur(Pioche pSprinteur) {
		this.pSprinteur = pSprinteur;
	}

	public Cartes[] piocherCartes() {
		Cartes[] tabCartes = new Cartes[4];
		try {
			if (pSprinteur.longueur() >= 4) {

				for (int i = 0; i < tabCartes.length; i++) {
					tabCartes[i] = pSprinteur.depiler();
				}
			} else {
				while (!defausseSprinteur.isEmpty()) {
					pSprinteur.empiler(defausseSprinteur.remove(defausseSprinteur.size() - 1));
				}
				Collections.shuffle(pSprinteur.getTasDeCartes());
				tabCartes = piocherCartes();
			}
		} catch (ExceptionPileVide e) {
			System.out.println("PileVide");
		}
		return tabCartes;
	}

	public int afficherChoisirCartes() {
		Cartes[] cartesPiochee = piocherCartes();
		for (int i = 0; i < cartesPiochee.length; i++) {
			System.out.println((i + 1) + " - Carte Sprinteur de valeur : " + cartesPiochee[i].getNbCases());
		}
		Scanner sc = new Scanner(System.in);
		int numCarteChoisie = 0;
		while (numCarteChoisie == 0) {
			System.out.println("Veuillez choisir une carte (1-4)");
			try {
				numCarteChoisie = sc.nextInt();
				sc.nextLine();
				while (numCarteChoisie < 1 || numCarteChoisie > 4) {
					System.out.println("Veuillez choisir une carte entre 1 et 4.");
					numCarteChoisie = sc.nextInt();
					sc.nextLine();
				}
			} catch (InputMismatchException e)

			{
				System.out.println("N'est pas un int.");
				sc.nextLine();
			}
		}
		int valeurCarte = -1;
		for (int i = 0; i < cartesPiochee.length; i++) {
			if (i == numCarteChoisie - 1)
				valeurCarte = cartesPiochee[i].getNbCases();
			else
				this.defausseSprinteur.add(cartesPiochee[i]);
		}
		return valeurCarte;
	}

	public void ajoutCarteFatigue() {
		CarteFatigue cf = new CarteFatigue(2);
		defausseSprinteur.add(cf);
	}

	public String toString() {
		String res = "";
		res += "Le sprinteur du joueur " + numequipe + " se trouvant � la case Numero " + getPositionActuelle();
		if (isFileDroite())
			res += " et est sur la file droite.";
		else
			res += " et est sur la file gauche.";
		return res;
	}

}