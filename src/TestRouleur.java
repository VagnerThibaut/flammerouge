import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

class TestRouleur {

	/**
	 * test le constructeur de la classe Rouleur
	 */
	@Test
	void testRouleur() {
		Rouleur r = new Rouleur(5, true, 1);
		assertEquals("La pile ne devrait pas �tre vide", false, r.getpRouleur().estPileVide());
		assertEquals("Le rouleur devrait �tre � la position 5", 5, r.getPositionActuelle());
	}

	/**
	 * test constructeur de la classe Sprinteur
	 */
	@Test
	void testSprinteur() {
		Sprinteur r = new Sprinteur(5, true, 1);
		assertEquals("La pile ne devrait pas �tre vide", false, r.getpSprinteur().estPileVide());
		assertEquals("Le Sprinteur devrait �tre � la position 5", 5, r.getPositionActuelle());
	}
}
