import java.util.ArrayList;
import java.util.Collections;

public class Pioche implements PilePioche {

	/**
	 * arraylist de cartes qui caract�rise la pioche
	 */
	private ArrayList<Cartes> tasDeCartes;

	/**
	 * constructeur qui cr�e une pioche pour les cyclistes
	 * 
	 * @param num,
	 *            entier 0 pour les rouleurs et 1 pour les sprinteurs
	 */
	public Pioche(int num) {
		tasDeCartes = new ArrayList<Cartes>();

		if (num == 0) {
			for (int i = 0; i < 3; i++) {
				tasDeCartes.add(new Cartes(3));
				tasDeCartes.add(new Cartes(4));
				tasDeCartes.add(new Cartes(5));
				tasDeCartes.add(new Cartes(6));
				tasDeCartes.add(new Cartes(7));
			}
		} else {
			for (int i = 0; i < 3; i++) {
				tasDeCartes.add(new Cartes(2));
				tasDeCartes.add(new Cartes(3));
				tasDeCartes.add(new Cartes(5));
				tasDeCartes.add(new Cartes(5));
				tasDeCartes.add(new Cartes(9));
			}
		}
		Collections.shuffle(tasDeCartes);
	}

	public boolean estPileVide() {
		return this.tasDeCartes.size() == 0;
	}

	public void empiler(Cartes c) {
		this.tasDeCartes.add(c);
	}

	public Cartes depiler() throws ExceptionPileVide {
		if (estPileVide())
			throw new ExceptionPileVide("PileVide");
		else
			return this.tasDeCartes.remove(tasDeCartes.size() - 1);
	}

	/**
	 * @return la pioche
	 */
	public ArrayList<Cartes> getTasDeCartes() {
		return this.tasDeCartes;
	}

	/**
	 * @return la longueur de la pioche
	 */
	public int longueur() {
		return this.tasDeCartes.size();
	}
}
