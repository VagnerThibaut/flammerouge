public interface PilePioche {
	/**
	 * @param c,
	 *            la carte � ajouter � la pioche
	 */
	public void empiler(Cartes c);

	/**
	 * @return la carte d�pil�e
	 * @throws ExceptionPileVide
	 */
	public Cartes depiler() throws ExceptionPileVide;

	/**
	 * @return boolean, � vrai si la pile est vide
	 */
	public boolean estPileVide();
}
