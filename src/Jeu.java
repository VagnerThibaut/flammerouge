import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Vagner Thibaut
 *
 */
/**
 * @author Missler Quentin
 *
 */
public class Jeu {
	/**
	 * tableaux des joueurs participant � la partie
	 */
	private Joueur[] Joueurs;
	/**
	 * Circuit sur lequel les joueurs vont s'affronter
	 */
	private Circuit circuit;
	/**
	 * le nombre de tour effectue
	 */
	private int tour;

	/**
	 * constructeur qui permet de creer le jeu
	 * 
	 * @param tabJ,
	 *            tableau stockant tous les noms des participants
	 * @param circuit,
	 *            idcircuit qu'a choisi les participants
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	public Jeu(String[] tabJ, int circuit) throws NomCircuitException, NomTuileException {

		this.Joueurs = new Joueur[tabJ.length];
		this.tour = 1;
		for (int i = 0; i < tabJ.length; i++) {
			this.Joueurs[i] = new Joueur(tabJ[i], i);
		}

		switch (circuit) {
		case 1:
			this.circuit = new Circuit("CB");
			break;
		case 2:
			this.circuit = new Circuit("CS");
			break;
		case 3:
			this.circuit = new Circuit("CM");
			break;
		case 4:
			this.circuit = new Circuit("CD");
			break;
		}
	}

	public Joueur[] getJoueurs() {
		return this.Joueurs;
	}

	public Circuit getCircuit() {
		return this.circuit;
	}

	/**
	 * fonction qui permet de compter le nombre de cycliste sur une case donnee
	 * 
	 * @param caseActuelle,
	 *            la fonction permet de compet le nombre de cycliste sur cette case
	 * @param tabPositionOccupees,
	 *            tableau d'entier qui stocke les positions des coureurs
	 * @return entier qui correspond aux nombres de cycliste sur une case donnee
	 */
	public int countCyclistOnCase(int caseActuelle, int[] tabPositionOccupees) {
		int res = 0;
		for (int i = 0; i < tabPositionOccupees.length; i++) {
			if (caseActuelle == tabPositionOccupees[i])
				res++;
		}
		return res;
	}

	/**
	 * fonction qui interagit avec l'utilisateur, elle permet de lui demander ou il
	 * veut placer ses coureurs
	 * 
	 * @param numjoueur,
	 *            id du joueur, permet aux joueurs de savoir c'est a qui de jouer
	 * @param s,
	 *            permet aux joueurs de savoir ou ils en sont, soit rouleur soit
	 *            sprinteur
	 * @param positionOccupees,
	 *            tableau d'entier qui stocke les positions des coureurs
	 * @return int, case sur laquelle l'utilisateur veut placer son cycliste
	 */
	public int demanderPlacer(int numjoueur, String s, int[] positionOccupees) {
		int place = 0;
		int nbcoureurs;
		Scanner sc = new Scanner(System.in);
		while (place == 0) {
			System.out.println("Joueur " + numjoueur + "\nO� voulez-vous placez votre " + s + " ? (1-5)");
			try {
				place = sc.nextInt();
				sc.nextLine();
				while (place < 1 || place > 5) {
					System.out.println("Erreur : Veuillez entrer une case comprise entre 1 et 5.");
					place = sc.nextInt();
					sc.nextLine();
				}
				nbcoureurs = countCyclistOnCase(place, positionOccupees);
				if (nbcoureurs == 0)
					switch (place) {
					case 1:
						place = 10;
						break;
					case 2:
						place = 20;
						break;
					case 3:
						place = 30;
						break;
					case 4:
						place = 40;
						break;
					case 5:
						place = 50;
						break;
					}
				if (nbcoureurs == 1) {
					System.out.println("La file de droite est occup�e, votre cycliste sera sur la file de gauche.\n");
					switch (place) {
					case 1:
						place = 11;
						break;
					case 2:
						place = 21;
						break;
					case 3:
						place = 31;
						break;
					case 4:
						place = 41;
						break;
					case 5:
						place = 51;
						break;
					}
				} else if (nbcoureurs == 2) {
					while (countCyclistOnCase(place, positionOccupees) == 2) {
						System.out.println(
								"Veuillez choisir une autre case car celle-ci est �j� occup�e par d'autres cyclistes.");
						try {
							place = sc.nextInt();
							sc.nextLine();
							while (place < 1 || place > 5) {
								System.out.println("Erreur : Veuillez entrer une case comprise entre 1 et 5.");
								place = sc.nextInt();
								sc.nextLine();
							}
						} catch (InputMismatchException e) {
							System.out.println("Veuillez entrer un entier.");
							sc.nextLine();
						}
					}
				}
			} catch (InputMismatchException e) {
				System.out.println("Veuillez entrer un entier.");
				sc.nextLine();
			}
		}
		return place;
	}

	/**
	 * fonction qui permet de recuperer toutes les positions des cyclistes
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 * @return tab int[], tableau d'entier qui stocke les positions des coureurs
	 */
	public int[] recupPositionOccupees(ArrayList<Cycliste> lc) {
		int[] positionOccupees = new int[8];
		int j = 0;
		for (int i = 0; i < lc.size(); i++) {
			positionOccupees[j] = lc.get(i).getPositionActuelle();
			j++;
		}
		return positionOccupees;
	}

	/**
	 * fonction qui permet de placer les cyclistes sur le circuit
	 * 
	 * @param nbjoueurs,
	 *            nb de joueurs dans la partie, cela permet de savoir combien de
	 *            fois on demande aux utilisateurs de placer leurs coureurs
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 */
	public void placerCoureur(int nbjoueurs, ArrayList<Cycliste> lc) {
		int placeRouleur;
		int placeSprinteur;
		for (int j = 0; j < nbjoueurs; j++) {
			int[] positionOccupees = recupPositionOccupees(lc);
			placeRouleur = demanderPlacer(j + 1, "Rouleur", positionOccupees);
			if (placeRouleur % 10 == 0)
				lc.add(new Rouleur(placeRouleur / 10, true, j + 1));
			else
				lc.add(new Rouleur(placeRouleur / 10, false, j + 1));

			positionOccupees = recupPositionOccupees(lc);
			placeSprinteur = demanderPlacer(j + 1, "Sprinteur", positionOccupees);

			if (placeSprinteur % 10 == 0)
				lc.add(new Sprinteur(placeSprinteur / 10, true, j + 1));
			else
				lc.add(new Sprinteur(placeSprinteur / 10, false, j + 1));
		}
	}

	/**
	 * fonction qui permet de recuperer le cycliste en tete
	 * 
	 * @param listcycl,
	 *            liste de cycliste qui stocke tous les cycliste
	 * @return cycliste qui est en tete
	 */
	public static Cycliste cyclisteEnTete(ArrayList<Cycliste> listcycl) {
		Cycliste tete = listcycl.get(0);
		for (int i = 1; i < listcycl.size(); i++) {
			if (listcycl.get(i).getPositionActuelle() > tete.getPositionActuelle())
				tete = listcycl.get(i);
		}
		return tete;
	}

	/**
	 * @return entier qui correspond aux nombres de joueurs dans la partie
	 */
	public static int setNbJoueur() {
		int nbJoueur = 0;
		Scanner sc = new Scanner(System.in);
		while (nbJoueur == 0) {
			System.out.println("Veuillez entrer le nombre de joueurs (entre 2 et 4) : ");
			try {
				int nbJ = sc.nextInt();
				sc.nextLine();
				while (nbJ < 2 || nbJ > 4) {
					System.out.println("Erreur : Nombre de joueur incorrect, entre 2 et 4 joueurs.");
					nbJ = sc.nextInt();
					sc.nextLine();
				}
				nbJoueur = nbJ;
			} catch (InputMismatchException e) {
				System.out.println("N'est pas un int.");
				sc.nextLine();
			}
		}
		return nbJoueur;
	}

	/**
	 * fonction qui permet de stocker le nom de chaque perticipants
	 * 
	 * @param nbJoueur,
	 *            entier qui correspond aux nombres de joueurs dans la partie
	 * @return tableau de String, qui stocke les noms de chaque participant
	 */
	public static String[] setParticipants(int nbJoueur) {
		Scanner sc = new Scanner(System.in);
		String[] tabJ = new String[nbJoueur];
		for (int i = 0; i < nbJoueur; i++) {
			System.out.println("Entrez le nom du joueur " + (i + 1) + " : ");
			tabJ[i] = sc.nextLine();
		}
		return tabJ;
	}

	/**
	 * @return l'id du circuit que les utilisateurs ont choisi
	 */
	public static int setCircuit() {
		Scanner sc = new Scanner(System.in);
		int idCircuit = 0;
		String[] tabCirc = { "1 - [Circuit de base]", "2 - [Circuit simple]", "3 - [Circuit moyen]",
				"4 - [Circuit difficile]" };
		while (idCircuit == 0) {
			System.out.println("Veuillez choisir un circuit (1-4)");
			for (String s : tabCirc) {
				System.out.println(s);
			}
			try {
				idCircuit = sc.nextInt();
				sc.nextLine();
			} catch (InputMismatchException e) {
				System.out.println("N'est pas un int.");
				sc.nextLine();
			}
		}
		return idCircuit;
	}

	/**
	 * fonction qui permet de faire avancer le cycliste en fonction du terrain, des
	 * autres cyclistes, et de la valeur de la carte piochee
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 * @param c,
	 *            cycliste a faire avancer
	 * @param val,
	 *            valeur de la carte choisie
	 */
	public void avancer(ArrayList<Cycliste> lc, Cycliste c, int val) {
		int numCrevaison = (int) (Math.random() * 20);
		if (numCrevaison != 1) {
			String type = "";
			if (c instanceof Rouleur)
				type = "Rouleur";
			else if (c instanceof Sprinteur)
				type = "Sprinteur";
			if (this.circuit.etatTuileAPos(c.getPositionActuelle()) == 1) {
				if (val > 5) {
					System.out.println(
							"----------------------------------------------------------------------------------------------");
					System.out.println(
							"--------------------------------  TERRAIN EN MONTEE  -----------------------------------------");
					System.out.println(
							"----------------------------------------------------------------------------------------------\n");
					val = 5;
					System.out.println("Carte du " + type + " du joueur " + c.getNumEquipe()
							+ " passee a 5 car il est situe en montee.\n");
				}
			} else if (this.circuit.etatTuileAPos(c.getPositionActuelle()) == 2) {
				if (val < 5) {
					System.out.println(
							"----------------------------------------------------------------------------------------------");
					System.out.println(
							"------------------------------  TERRAIN EN DESCENTE  -----------------------------------------");
					System.out.println(
							"----------------------------------------------------------------------------------------------\n");
					val = 5;
					System.out.println("Carte du " + type + " du joueur " + c.getNumEquipe()
							+ " passee a 5 car il est situe en descente.\n");
				}
			}
			int nbJ = 0;
			for (int i = 0; i < lc.size(); i++) {
				if (lc.get(i).getPositionActuelle() == (c.getPositionActuelle() + val))
					nbJ++;
			}
			if (nbJ == 0) {
				c.setPositionActuelle((c.getPositionActuelle() + val));
				c.setFileDroite(true);
			} else if (nbJ == 1) {
				c.setPositionActuelle((c.getPositionActuelle() + val));
				c.setFileDroite(false);
			} else if (nbJ == 2) {
				avancer(lc, c, val - 1);
			}
		} else {
			System.out.println(
					"----------------------------------------------------------------------------------------------");
			System.out.println(
					"-----------------------------  CREVAISON DANS LE PELOTON  ------------------------------------");
			System.out.println(
					"----------------------------------------------------------------------------------------------\n");
			String type = "";
			if (c instanceof Rouleur)
				type = "Rouleur";
			else if (c instanceof Sprinteur)
				type = "Sprinteur";
			System.out.println("Le " + type + " de l'�quipe " + c.getNumEquipe()
					+ " est victime d'une crevaison, il repartira au prochain tour\n");
		}
	}

	/**
	 * @param lc,
	 *            liste de cyclistes qui stocke tous les cycliste
	 * @return liste de cyclistes triee du premier au dernier
	 */
	public ArrayList<Cycliste> trierCycliste(ArrayList<Cycliste> lc) {
		Collections.sort(lc, new cyclisteComparator());
		return lc;
	}

	/**
	 * fonction qui permet de faire une belle sortie console avec l'etat du jeu
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 */
	public void afficherPositionCycliste(ArrayList<Cycliste> lc) {
		System.out.println(
				"----------------------------------------------------------------------------------------------");
		System.out.println(
				"--------------------------------  ETAT DES COUREURS  -----------------------------------------");
		System.out.println(
				"----------------------------------------------------------------------------------------------\n");
		System.out.println("Le cycliste en t�te de la course est " + Jeu.cyclisteEnTete(lc) + "\n");
		for (int i = 1; i < lc.size(); i++)
			System.out.println(lc.get(i));
		System.out.println(
				"\n----------------------------------------------------------------------------------------------");
		System.out.println("-----------------------------------  TOUR N� " + this.tour
				+ "  ----------------------------------------------");
		System.out.println(
				"----------------------------------------------------------------------------------------------\n");
	}

	/**
	 * fonction qui permet de connaitre la position des cycliste la plus avancee
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 * @return entier qui correspond a la case ou se trouve le cycliste en tete
	 */
	public int posMax(ArrayList<Cycliste> lc) {
		int max = 0;
		for (int i = 0; i < lc.size(); i++) {
			if (lc.get(i).getPositionActuelle() > max)
				max = lc.get(i).getPositionActuelle();
		}
		return max;
	}

	/**
	 * fonction qui permet d'appliquer l'aspiration
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 */
	public void appliquerAspiration(ArrayList<Cycliste> lc) {

		int[] positionsOccupees;

		for (int i = lc.size() - 1; i > 0; i--) {
			positionsOccupees = recupPositionOccupees(lc);
			int numCaseJ1 = lc.get(i).getPositionActuelle();

			int nbjouOnCase = countCyclistOnCase(numCaseJ1, positionsOccupees);
			// si il y a un cycliste dans le groupe
			if (nbjouOnCase == 1) {
				// test si il y a personne sur la case d'apr�s et si il y a au moins un coureur
				// sur la case d'apr�s
				if (countCyclistOnCase((numCaseJ1 + 1), positionsOccupees) == 0
						&& ((countCyclistOnCase(numCaseJ1 + 2, positionsOccupees) == 1
								|| countCyclistOnCase(numCaseJ1 + 2, positionsOccupees) == 2))) {
					// on fait avancer tout le groupe
					int iteBoucle = 1;
					int k = 1;
					lc.get(i).setPositionActuelle(lc.get(i).getPositionActuelle() + 1);
					while (countCyclistOnCase(numCaseJ1 - iteBoucle, positionsOccupees) > 0) {
						// test si il y a une ou 2 personne sur les cases derri�re
						if (countCyclistOnCase(numCaseJ1 - iteBoucle, positionsOccupees) == 1) {
							lc.get(i + k).setPositionActuelle(lc.get(i + k).getPositionActuelle() + 1);
							k++;
						} else {
							lc.get(i + k).setPositionActuelle(lc.get(i + k).getPositionActuelle() + 1);
							k++;
							lc.get(i + k).setPositionActuelle(lc.get(i + k).getPositionActuelle() + 1);
							k++;
						}
						iteBoucle++;
					}
				}
			} // si il y a 2 cyclistes dans le groupe
			else if (nbjouOnCase == 2) {
				if (countCyclistOnCase((numCaseJ1 + 1), positionsOccupees) == 0
						&& ((countCyclistOnCase(numCaseJ1 + 2, positionsOccupees) == 1
								|| countCyclistOnCase(numCaseJ1 + 2, positionsOccupees) == 2))) {
					// on fait avancer tout le groupe
					// aide : lc.get(0) c'est le coureur en tete, lc.get(last) c'est le dernier
					// coureur
					int iteBoucle = 1;
					int k = 1;
					lc.get(i).setPositionActuelle(lc.get(i).getPositionActuelle() + 1);
					lc.get(i - 1).setPositionActuelle(lc.get(i - 1).getPositionActuelle() + 1);
					while (countCyclistOnCase(numCaseJ1 - iteBoucle, positionsOccupees) > 0) {
						// test si il y a une ou 2 personne sur les cases derri�re
						if (countCyclistOnCase(numCaseJ1 - iteBoucle, positionsOccupees) == 1) {
							lc.get(i + k).setPositionActuelle(lc.get(i + k).getPositionActuelle() + 1);
							k++;
						} else {
							lc.get(i + k).setPositionActuelle(lc.get(i + k).getPositionActuelle() + 1);
							k++;
							lc.get(i + k).setPositionActuelle(lc.get(i + k).getPositionActuelle() + 1);
							k++;
						}
						iteBoucle++;
					}
				}
			}
		}

	}

	/**
	 * fonction qui permet d'appliquer la fatigue
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 */
	public void appliquerFatigue(ArrayList<Cycliste> lc) {

		int[] positionsOccupees;

		for (int i = lc.size() - 1; i >= 0; i--) {
			positionsOccupees = recupPositionOccupees(lc);
			int numCaseJ1 = lc.get(i).getPositionActuelle();

			if (countCyclistOnCase((numCaseJ1 + 1), positionsOccupees) == 0
					&& ((countCyclistOnCase(numCaseJ1 + 2, positionsOccupees) == 0))) {
				lc.get(i).ajoutCarteFatigue();
			}
		}
	}

	/**
	 * fonction qui permet d'afficher a la console le gagnant de la partie
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 */
	public void afficherRes(ArrayList<Cycliste> lc) {
		String nom = this.Joueurs[lc.get(0).getNumEquipe() - 1].getNomJoueur();
		System.out.println("Fin de la partie bravo au joueur " + lc.get(0).getNumEquipe() + "(" + nom
				+ ") qui a gagn�e la partie !");
	}

	/**
	 * fonction qui permet de stocker dans un fichier les informations du debut de
	 * la partie
	 * 
	 * @throws IOException
	 */
	public void enreDebut() {
		try {
			FileWriter fich = new FileWriter("infoJeu.txt");
			fich.write("Nombre de joueurs : " + this.Joueurs.length + "\n");
			for (int i = 0; i < this.Joueurs.length; i++) {
				fich.write((i + 1) + " - " + this.Joueurs[i].getNomJoueur() + "\n");
			}
			fich.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * fonction qui permet de stocker dans un fichier les informations de la fin de
	 * la partie
	 * 
	 * @param lc,
	 *            liste de cycliste qui stocke tous les cycliste
	 * @throws IOException
	 */
	public void enreStat(ArrayList<Cycliste> lc) {
		try {
			FileWriter fich = new FileWriter("statJeu.txt");
			fich.write("Resultat du dernier jeu : \n");
			String type = "";
			String nom = "";
			for (int i = 0; i < lc.size(); i++) {
				nom = this.Joueurs[lc.get(i).getNumEquipe() - 1].getNomJoueur();
				if (lc.get(i) instanceof Rouleur)
					type = "Rouleur";
				if (lc.get(i) instanceof Sprinteur)
					type = "Sprinteur";
				fich.write((i + 1) + " - " + type + " du joueur n�" + lc.get(i).getNumEquipe() + "(" + nom
						+ ") avec un score de : " + lc.get(i).getPositionActuelle() + "\n");
			}
			fich.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * main a executer pour lancer le jeu
	 * 
	 * @param args
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 * @throws IOException
	 */
	public static void main(String[] args) throws NomCircuitException, NomTuileException, IOException {
		System.out.println("Bienvenue dans votre jeu FLAMME ROUGE, bon jeu !\n");
		boolean recommencer = true;
		while (recommencer) {
			int nbJoueur = setNbJoueur();

			String[] tabJ = setParticipants(nbJoueur);

			int idCircuit = setCircuit();

			System.out.println("Nouveau Jeu creer avec " + tabJ.length + " joueurs :");
			for (int i = 0; i < tabJ.length; i++) {
				System.out.println((i + 1) + " - " + tabJ[i]);
			}
			Jeu j = new Jeu(tabJ, idCircuit);
			System.out.println("Qui vont s'affronter sur le circuit : " + j.circuit.getNomCircuit() + "\n");
			j.enreDebut();
			ArrayList<Cycliste> ListCyclistes = new ArrayList<Cycliste>();

			switch (nbJoueur) {
			case 2:
				j.placerCoureur(2, ListCyclistes);
				break;
			case 3:
				j.placerCoureur(3, ListCyclistes);
				break;
			case 4:
				j.placerCoureur(4, ListCyclistes);
				break;
			}
			System.out.print("\n");
			j.afficherPositionCycliste(ListCyclistes);
			j.tour++;
			j.trierCycliste(ListCyclistes);

			while (j.posMax(ListCyclistes) < j.circuit.getLongueur()) {
				int[][] tabValeurCartes = new int[nbJoueur][2];
				int numeq;
				for (int i = 0; i < ListCyclistes.size(); i++) {
					numeq = ListCyclistes.get(i).getNumEquipe();
					System.out.println("Joueur numero " + numeq + ", veuillez choisir votre carte :");
					if (tabValeurCartes[numeq - 1][0] == 0)
						tabValeurCartes[numeq - 1][0] = ListCyclistes.get(i).afficherChoisirCartes();
					else
						tabValeurCartes[numeq - 1][1] = ListCyclistes.get(i).afficherChoisirCartes();
				}
				System.out.println(
						"\n----------------------------------------------------------------------------------------------");
				System.out.println(
						"-------------------------------  CARTES DES JOUEURS  -----------------------------------------");
				System.out.println(
						"----------------------------------------------------------------------------------------------\n");
				for (int i = 0; i < tabValeurCartes.length; i++) {
					System.out.print("Le rouleur du joueur " + (i + 1) + " poss�de une carte de valeur ");
					System.out.println(tabValeurCartes[i][0]);
					System.out.print("Le sprinteur du joueur " + (i + 1) + " poss�de une carte de valeur ");
					System.out.println(tabValeurCartes[i][1]);
				}
				System.out.print("\n");
				int type = 0;
				int numequipe;
				for (int i = 0; i < ListCyclistes.size(); i++) {
					Cycliste c = ListCyclistes.get(i);
					numequipe = ListCyclistes.get(i).getNumEquipe();
					if (c instanceof Rouleur)
						type = 0;
					else
						type = 1;
					j.avancer(ListCyclistes, ListCyclistes.get(i), tabValeurCartes[numequipe - 1][type]);
				}
				j.trierCycliste(ListCyclistes);
				j.appliquerAspiration(ListCyclistes);
				j.appliquerFatigue(ListCyclistes);
				j.afficherPositionCycliste(ListCyclistes);
				j.tour++;
			}
			j.afficherRes(ListCyclistes);
			j.enreStat(ListCyclistes);
			Scanner sc = new Scanner(System.in);
			int reco = -1;
			while (reco == -1) {
				System.out.println("Voulez-vous recommencer une partie ?\n 0 - non\n 1 - oui");
				try {
					int reco2 = sc.nextInt();
					sc.nextLine();
					while (reco2 != 0 && reco2 != 1) {
						System.out.println("Veuillez entrer 0 ou 1");
						reco2 = sc.nextInt();
						sc.nextLine();
					}
					reco = reco2;
				} catch (InputMismatchException e) {
					System.out.println("N'est pas un entier");
					sc.nextLine();
				}
			}
			if (reco == 0)
				recommencer = false;
		}
	}
}
