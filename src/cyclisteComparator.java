import java.util.Comparator;

public class cyclisteComparator implements Comparator<Cycliste> {

	public int compare(Cycliste c1, Cycliste c2) {
		return c2.getPositionActuelle() - c1.getPositionActuelle();
	}

}
