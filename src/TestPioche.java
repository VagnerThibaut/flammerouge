import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class TestPioche {

	/**
	 * test toutes les m�thodes de la classe Pioche
	 * 
	 * @throws ExceptionPileVide
	 */
	@Test
	void testPiocheDeBase() throws ExceptionPileVide {
		Pioche p = new Pioche(0);

		assertEquals("la longueur de la pioche devrait etre �gale � 15", 15, p.longueur());
		assertEquals("la pile ne doit pas etre vide", false, p.estPileVide());
		Cartes c = new Cartes(10);
		p.empiler(c);
		assertEquals("La longueur de la pioche devrait etre �gale � 16", 16, p.longueur());
		Cartes c1 = p.depiler();
		assertEquals("la valeur de la carte devrait etre �gale a 10", 10, c1.getNbCases());
		assertEquals("la longueur devrait etre �gale � 15", 15, p.longueur());
		for (int i = 0; i < 15; i++) {
			p.depiler();
		}
		assertEquals("la pile doit etre vide", true, p.estPileVide());
	}

	/**
	 * test de la pioche avec un rouleur
	 * 
	 * @throws ExceptionPileVide
	 */
	@Test
	void testPiocheCycliste() throws ExceptionPileVide {
		Rouleur r = new Rouleur(5, true, 1);
		Pioche pr = r.getpRouleur();
		assertEquals("la longueur de la pioche devrait etre �gale � 15", 15, pr.longueur());
		assertEquals("la pile ne doit pas etre vide", false, pr.estPileVide());
		Cartes[] tabC = new Cartes[4];
		tabC = r.piocherCartes();
		ArrayList<Cartes> lc = new ArrayList<Cartes>();
		lc = r.getDefausseRouleur();
		assertEquals("la longueur de la pioche devrait �tre �gale � 11", 11, pr.longueur());

	}

	/**
	 * test la methode ajoutCarteFatigue de la classe Cycliste
	 */
	@Test
	void testAjoutCarteFatigue() {
		Rouleur r = new Rouleur(5, true, 1);
		ArrayList<Cartes> lc = new ArrayList<Cartes>();
		r.ajoutCarteFatigue();
		lc = r.getDefausseRouleur();
		assertEquals("la longueur de la defausse devrait �tre �gale � 1", 1, lc.size());
		assertEquals("la valeur devrait etre �gale � 2", 2, lc.remove(0).getNbCases());
	}
}
