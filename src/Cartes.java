public class Cartes {
	/**
	 * Valeur de la carte
	 */
	private int nbCases;

	/**
	 * @param num,
	 *            valeur de la carte � ajouter
	 */
	public Cartes(int num) {
		this.nbCases = num;
	}

	public int getNbCases() {
		return this.nbCases;
	}

	public void setNbCases(int nbCases) {
		this.nbCases = nbCases;
	}

	public String toString() {
		return ("Carte de valeur : " + this.nbCases);
	}
}
