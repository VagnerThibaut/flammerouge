import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Rouleur extends Cycliste {
	/**
	 * arraylist de carte qui contient les cartes jouees
	 */
	private ArrayList<Cartes> defausseRouleur;
	/**
	 * pioche du rouleur
	 */
	private Pioche pRouleur;

	/**
	 * Constructeur
	 * 
	 * @param pos,
	 *            position du rouleur au d�but
	 * @param fileDr,
	 *            � vrai si le rouleur est sur la file de droite
	 * @param numeq,
	 *            num�ro de l'�quipe du rouleur
	 */
	public Rouleur(int pos, boolean fileDr, int numeq) {
		super(pos, fileDr, numeq);
		pRouleur = new Pioche(0);
		defausseRouleur = new ArrayList<Cartes>();
	}

	/**
	 * @return la defausse du rouleur
	 */
	public ArrayList<Cartes> getDefausseRouleur() {
		return defausseRouleur;
	}

	/**
	 * @param defausseRouleur
	 */
	public void setDefausseRouleur(ArrayList<Cartes> defausseRouleur) {
		this.defausseRouleur = defausseRouleur;
	}

	/**
	 * @return la pioche du rouleur
	 */
	public Pioche getpRouleur() {
		return pRouleur;
	}

	/**
	 * @param pRouleur,
	 *            la pioche du rouleur
	 */
	public void setpRouleur(Pioche pRouleur) {
		this.pRouleur = pRouleur;
	}

	public Cartes[] piocherCartes() {
		Cartes[] tabCartes = new Cartes[4];

		try {
			if (pRouleur.longueur() >= 4) {
				for (int i = 0; i < tabCartes.length; i++) {
					tabCartes[i] = pRouleur.depiler();
				}
			} else {
				while (!defausseRouleur.isEmpty()) {
					pRouleur.empiler(defausseRouleur.remove(defausseRouleur.size() - 1));
				}
				Collections.shuffle(pRouleur.getTasDeCartes());
				tabCartes = piocherCartes();
			}
		} catch (Exception e) {
			System.out.println("PileVide");
		}
		return tabCartes;
	}

	public int afficherChoisirCartes() {
		Cartes[] cartesPiochee = piocherCartes();
		for (int i = 0; i < cartesPiochee.length; i++) {
			System.out.println((i + 1) + " - Carte Rouleur de valeur : " + cartesPiochee[i].getNbCases());
		}
		Scanner sc = new Scanner(System.in);
		int numCarteChoisie = 0;
		while (numCarteChoisie == 0) {
			System.out.println("Veuillez choisir une carte (1-4)");
			try {
				numCarteChoisie = sc.nextInt();
				sc.nextLine();
				while (numCarteChoisie < 1 || numCarteChoisie > 4) {
					System.out.println("Veuillez choisir une carte entre 1 et 4.");
					numCarteChoisie = sc.nextInt();
					sc.nextLine();
				}
			} catch (InputMismatchException e) {
				System.out.println("N'est pas un int.");
				sc.nextLine();
			}
		}
		int valeurCarte = -1;
		for (int i = 0; i < cartesPiochee.length; i++) {
			if (i == numCarteChoisie - 1) {
				valeurCarte = cartesPiochee[i].getNbCases();
			} else {
				defausseRouleur.add(cartesPiochee[i]);
			}
		}
		return valeurCarte;
	}

	public void ajoutCarteFatigue() {
		CarteFatigue cf = new CarteFatigue(2);
		defausseRouleur.add(cf);
	}

	public String toString() {
		String res = "";
		res += "Le rouleur du joueur " + numequipe + " se trouvant � la case Num�ro " + getPositionActuelle();
		if (isFileDroite())
			res += " et est sur la file droite.";
		else
			res += " et est sur la file gauche.";
		return res;
	}

}
