public class Circuit {
	/**
	 * nom du circuit
	 */
	private String nomCircuit;
	/**
	 * tableau de type tuile qui va permettre de construire le jeu
	 */
	private Tuile[] tabTuiles;
	/**
	 * longueur du circuit
	 */
	private int longueur;
	private Tuile[] circuitbase = { new Tuile(0, "D"), new Tuile(2, "LD"), new Tuile(0, "LD"), new Tuile(0, "VL"),
			new Tuile(1, "VL"), new Tuile(0, "LD"), new Tuile(0, "LD"), new Tuile(0, "VL"),	new Tuile(0, "LD"), 
			new Tuile(0, "VS"), new Tuile(0, "LD"), new Tuile(1, "VL"), new Tuile(0, "A") };
	
	private Tuile[] circuitsimple = { new Tuile(0, "D"), new Tuile(2, "VS"), new Tuile(0, "LD"), new Tuile(2, "VL"),
			new Tuile(1, "LD"), new Tuile(0, "LD"), new Tuile(2, "LD"),	new Tuile(0, "VL"), new Tuile(0, "LD"),
			new Tuile(0, "LD"), new Tuile(2, "VL"), new Tuile(0, "A") };
	
	private Tuile[] circuitmoyen = { new Tuile(0, "D"), new Tuile(0, "VS"), new Tuile(0, "LD"), new Tuile(1, "VL"),
			new Tuile(2, "VL"), new Tuile(2, "VS"), new Tuile(0, "LD"), new Tuile(0, "VL"), new Tuile(0, "VS"),
			new Tuile(2, "VL"), new Tuile(1, "LD"),	new Tuile(1, "VS"), new Tuile(0, "VS"), new Tuile(0, "LD"), new Tuile(1, "VL"), new Tuile(0, "A") };
	
	private Tuile[] circuitdiff = { new Tuile(0, "D"), new Tuile(1, "VS"), new Tuile(1, "LD"), new Tuile(0, "VL"),
			new Tuile(1, "LD"), new Tuile(0, "VS"), new Tuile(0, "LD"), new Tuile(2, "VL"), new Tuile(0, "VL"),
			new Tuile(0, "VL"), new Tuile(1, "VS"),	new Tuile(2, "VS"), new Tuile(1, "VS"), new Tuile(1, "LD"), new Tuile(1, "VL"), new Tuile(0, "A") };

	/**
	 * Constructeur de la classe circuit en fonction du nom du circuit
	 * 
	 * @param nC
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	public Circuit(String nC) throws NomCircuitException, NomTuileException {
		if (!nC.equals("CB") && !nC.equals("CS") && !nC.equals("CM") && !nC.equals("CD")
				&& !nC.equals("RVW")) {
			throw new NomCircuitException();
		}

		switch (nC) {
			case "CB":
				this.tabTuiles = circuitbase;
				this.nomCircuit = "Circuit de base";
				break;
			case "CS":
				this.tabTuiles = circuitsimple;
				this.nomCircuit = "Circuit simple";
				break;
			case "CM":
				this.tabTuiles = circuitmoyen;
				this.nomCircuit = "Circuit moyen";
				break;
			case "CD":
				this.tabTuiles = circuitdiff;
				this.nomCircuit = "Circuit difficile";
			default:
				this.tabTuiles = circuitbase;
				this.nomCircuit = "Circuit de base";
			}
		this.longueur = this.longCircuit();
	}

	/**
	 * Return l'etat de pente de la tuile a la position pos
	 * 
	 * @param pos
	 * @return
	 */
	public int etatTuileAPos(int pos) {
		int res = -1;
		int posAct = 0;
		int i = 0;
		while(res == -1 && i < this.tabTuiles.length)
		{
			posAct += this.tabTuiles[i].longT();
			if(this.tabTuiles[i].longT() == 6)
			{
				if(pos >= posAct-6 && pos <= posAct) res = tabTuiles[i].etat();
			}
			else if(this.tabTuiles[i].longT() == 2)
			{
				if(pos >= posAct-2 && pos <= posAct) res = tabTuiles[i].etat();
			}
			i++;
		}
		return res;
	}
	
	/**
	 * fonction qui permet de conna�tre la longueur d'un circuit la longueur du circuit
	 * 
	 * @return int
	 */
	private int longCircuit()
	{
		int longueur = 0;
		for(int i = 0; i <this.tabTuiles.length;i++)
		{
			longueur += this.tabTuiles[i].longT();
		}
		return longueur;
	}
	
	public String getNomCircuit() {
		return this.nomCircuit;
	}

	public Tuile[] getTabTuiles() {
		return tabTuiles;
	}

	public void setTabTuiles(Tuile[] tabTuiles) {
		this.tabTuiles = tabTuiles;
	}

	public int getLongueur() {
		return this.longueur;
	}
}
