public class Joueur {

	/**
	 * nom du joueur
	 */
	private String nomJoueur;
	/**
	 * num�ro de l'�quipe du joueur
	 */
	private int numJoueur;

	/**
	 * @return nom du joueur
	 */
	public String getNomJoueur() {
		return this.nomJoueur;
	}

	/**
	 * @param nomJoueur,
	 *            nom du joueur
	 */
	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	/**
	 * @return num�ro de l'�quipe
	 */
	public int getNumJoueur() {
		return this.numJoueur;
	}

	/**
	 * @param numJoueur
	 *            le num�ro de l'�quipe
	 */
	public void setNumJoueur(int numJoueur) {
		this.numJoueur = numJoueur;
	}

	/**
	 * Constructor
	 * 
	 * @param nJ,
	 *            nom du joueur
	 * @param num,
	 *            num�ro de l'�quipe
	 */
	public Joueur(String nJ, int num) {
		this.nomJoueur = nJ;
		this.numJoueur = num;
	}
}
