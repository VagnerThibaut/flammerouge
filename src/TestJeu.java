import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * @author Thibaut Vagner
 * @author Quentin Missler
 *
 */
class TestJeu {

	String[] tabj = { "thib", "quentin", "paul", "jean" };

	/**
	 * Test la m�thode avancer de la classe jeu
	 * 
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	@Test
	void testAvancer() throws NomCircuitException, NomTuileException {

		Jeu j = new Jeu(tabj, 1);
		Cycliste c = new Rouleur(2, true, 1);
		Cycliste c1 = new Rouleur(3, true, 1);
		Cycliste c2 = new Rouleur(4, false, 1);
		Cycliste c3 = new Rouleur(5, false, 1);
		Cycliste c4 = new Rouleur(1, true, 1);

		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(c);
		lc.add(c1);
		lc.add(c2);
		lc.add(c3);
		lc.add(c4);

		j.avancer(lc, c, 5);
		// test sans coureur sur la case o� le cycliste en question doit arriver
		assertEquals("le cycliste devrait etre � la case 7", 7, c.getPositionActuelle());
		assertEquals("le cycliste devrait etre sur la file droite", true, c.isFileDroite());

		// test avec un coureur �tant sur la file droite sur la case o� le cycliste doit
		// arriver
		c1.setPositionActuelle(7);
		c.setPositionActuelle(2);
		j.avancer(lc, c, 5);
		assertEquals("le cycliste devrait etre � la case 7", 7, c.getPositionActuelle());
		assertEquals("le cycliste devrait etre sur la file gauche", false, c.isFileDroite());

		// test avec deux coureurs sur la case o� doit arriver le cycliste
		c.setPositionActuelle(2);
		c2.setPositionActuelle(7);
		j.avancer(lc, c, 5);
		assertEquals("le cycliste devrait etre � la case 6", 6, c.getPositionActuelle());
		assertEquals("le cycliste devrait etre sur la file droite", true, c.isFileDroite());

		// test avec deux coureurs sur la case o� doit arriver le cycliste et un coureur
		// juste
		// derri�re sur la file droite
		c.setPositionActuelle(2);
		c4.setPositionActuelle(6);
		c4.setFileDroite(true);
		j.avancer(lc, c, 5);
		assertEquals("le cycliste devrait etre � la case 6", 6, c.getPositionActuelle());
		assertEquals("le cycliste devrait etre sur la file gauche", false, c.isFileDroite());

		// test avec deux coureurs sur la case o� doit arriver le cycliste et 2 coureurs
		// juste
		// derriere cette case, le cycliste va donc s'arr�ter 2 cases avant de se qui
		// avait
		// �t� pr�vu
		c.setPositionActuelle(2);
		c3.setPositionActuelle(6);
		j.avancer(lc, c, 5);
		assertEquals("le cycliste devrait etre � la case 5", 5, c.getPositionActuelle());
		assertEquals("le cycliste devrait etre sur la file droite", true, c.isFileDroite());

		c.setPositionActuelle(10);
		c1.setPositionActuelle(6);
		c2.setPositionActuelle(8);
		c3.setPositionActuelle(7);
		c4.setPositionActuelle(6);

		j.trierCycliste(lc);
		int[] tab = j.recupPositionOccupees(lc);
		assertEquals("erreur", 6, tab[3]);
		assertEquals("erreur", 6, tab[4]);
	}

	/**
	 * test la m�thode aspiration de la classe Jeu
	 * 
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	@Test
	void testAppliquerAspiration() throws NomCircuitException, NomTuileException {

		Jeu j = new Jeu(tabj, 1);
		Cycliste c = new Rouleur(7, true, 1);
		Cycliste c1 = new Rouleur(5, true, 1);
		Cycliste c2 = new Rouleur(4, true, 1);
		Cycliste c3 = new Rouleur(4, false, 1);
		Cycliste c4 = new Rouleur(3, true, 1);
		Cycliste c5 = new Rouleur(5, false, 1);
		Cycliste c6 = new Rouleur(0, true, 1);
		Cycliste c7 = new Rouleur(0, false, 1);

		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(c);
		lc.add(c1);
		lc.add(c2);
		lc.add(c3);
		lc.add(c4);
		lc.add(c5);
		lc.add(c6);
		lc.add(c7);
		j.trierCycliste(lc);
		j.appliquerAspiration(lc);
		assertEquals("c1 devrait �tre � la case 6", 6, c1.getPositionActuelle());
		assertEquals("c2 devrait �tre � la case 5", 5, c2.getPositionActuelle());
		assertEquals("c3 devrait �tre � la case 5", 5, c3.getPositionActuelle());
		assertEquals("c3 devrait �tre sur la file gauche", false, c3.isFileDroite());
		assertEquals("c4 devrait �tre � la case 4", 4, c4.getPositionActuelle());
		assertEquals("c5 devrait �tre � la case 6", 6, c5.getPositionActuelle());

		assertEquals("c6 devrait �tre � la case 0", 0, c6.getPositionActuelle());
		assertEquals("c7 devrait �tre � la case 0", 0, c7.getPositionActuelle());
	}

	/**
	 * test la m�thode aspiration de la classe Jeu
	 * 
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	@Test
	void testAppliquerAspiration2() throws NomCircuitException, NomTuileException {

		Jeu j = new Jeu(tabj, 1);
		Cycliste c = new Rouleur(9, true, 1);
		Cycliste c1 = new Rouleur(7, true, 1);
		Cycliste c2 = new Rouleur(7, false, 1);
		Cycliste c3 = new Rouleur(6, true, 1);
		Cycliste c4 = new Rouleur(4, true, 1);
		Cycliste c5 = new Rouleur(4, false, 1);
		Cycliste c6 = new Rouleur(1, true, 1);
		Cycliste c7 = new Rouleur(1, false, 1);

		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(c);
		lc.add(c1);
		lc.add(c2);
		lc.add(c3);
		lc.add(c4);
		lc.add(c5);
		lc.add(c6);
		lc.add(c7);
		j.trierCycliste(lc);
		j.appliquerAspiration(lc);
		assertEquals("c devrait pas bouger", 9, c.getPositionActuelle());
		assertEquals("c1 devrait �tre � la case 8", 8, c1.getPositionActuelle());
		assertEquals("c2 devrait �tre � la case 8", 8, c2.getPositionActuelle());
		assertEquals("c2 devrait �tre sur la file gauche", false, c2.isFileDroite());
		assertEquals("c3 devrait �tre � la case 7", 7, c3.getPositionActuelle());
		assertEquals("c4 devrait �tre � la case 6", 6, c4.getPositionActuelle());
		assertEquals("c5 devrait �tre � la case 6", 6, c5.getPositionActuelle());

		assertEquals("c6 devrait �tre � la case 1", 1, c6.getPositionActuelle());
		assertEquals("c7 devrait �tre � la case 1", 1, c7.getPositionActuelle());
	}

	/**
	 * test la m�thode appliquerFatigue de la classe Jeu
	 * 
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	@Test
	void testAppliquerFatigue() throws NomCircuitException, NomTuileException {

		Jeu j = new Jeu(tabj, 1);
		Cycliste c = new Rouleur(9, true, 1);
		Cycliste c1 = new Rouleur(7, true, 1);
		Cycliste c2 = new Rouleur(7, false, 1);
		Cycliste c3 = new Rouleur(6, true, 1);
		Cycliste c4 = new Rouleur(4, true, 1);
		Cycliste c5 = new Rouleur(4, false, 1);
		Cycliste c6 = new Rouleur(1, true, 1);
		Cycliste c7 = new Rouleur(1, false, 1);

		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(c);
		lc.add(c1);
		lc.add(c2);
		lc.add(c3);
		lc.add(c4);
		lc.add(c5);
		lc.add(c6);
		lc.add(c7);
		assertEquals("la longueur de la defausse devrait �gale � 0", 0, ((Rouleur) c6).getDefausseRouleur().size());
		j.appliquerFatigue(lc);
		assertEquals("longueur de la pioche �gale � 15", 15, ((Rouleur) c).getpRouleur().longueur());
		assertEquals("la longueur de la pioche devrait �gale � 15", 15, ((Rouleur) c6).getpRouleur().longueur());
		assertEquals("la longueur de la defausse devrait �gale � 1", 1, ((Rouleur) c6).getDefausseRouleur().size());
		assertEquals("la longueur de la defausse devrait �gale � 1", 1, ((Rouleur) c7).getDefausseRouleur().size());
		assertEquals("la longueur de la defausse devrait �gale � 1", 1, ((Rouleur) c).getDefausseRouleur().size());
	}

	/**
	 * Test les mont�es et les descentes
	 * 
	 * @throws NomCircuitException
	 * @throws NomTuileException
	 */
	@Test
	void testTerrain() throws NomCircuitException, NomTuileException {
		Jeu j = new Jeu(tabj, 3);
		Cycliste c = new Rouleur(13, true, 1);
		Cycliste c1 = new Rouleur(15, true, 1);
		Cycliste c2 = new Rouleur(17, true, 1);
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(c);
		lc.add(c1);
		lc.add(c2);
		j.avancer(lc, c, 7);
		j.avancer(lc, c1, 9);
		j.avancer(lc, c2, 2);
		assertEquals("le rouleur devrait etre a la position 20", 20, c.getPositionActuelle());
		assertEquals("le rouleur devrait etre a la position 20", 20, c1.getPositionActuelle());
		assertEquals("le rouleur devrait etre a la position 22", 22, c2.getPositionActuelle());
	}

}
