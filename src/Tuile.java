public class Tuile {
	/**
	 * 0 s'il n'y a pas de pente, 1 si c'est une mont�e et 2 si c'est une descente
	 */
	int pente;
	/**
	 * nom de la tuile
	 */
	String nomTuile;

	/**
	 * Constructeur
	 * 
	 * @param p
	 * @param nT
	 * @throws NomTuileException
	 */
	public Tuile(int p, String nT) throws NomTuileException {
		if (p < 0 || p > 2) {
			p = 0;
			System.out.println("Erreur : Valeur de pente incorrect, mise par defaut a 0.");
		}
		this.pente = p;

		if (!nT.equals("VL") && !nT.equals("VS") && !nT.equals("D") && !nT.equals("A") && !nT.equals("LD")) {
			throw new NomTuileException();
		}
		this.nomTuile = nT;
	}

	/**
	 * @return longueur de la tuile
	 */
	public int longT() {
		int nbCases = 0;
		switch (this.nomTuile) {
		case "VL":
			nbCases = 2;
			break;
		case "VS":
			nbCases = 2;
			break;
		case "D":
			nbCases = 6;
			break;
		case "A":
			nbCases = 6;
			break;
		case "LD":
			nbCases = 6;
			break;
		}
		return nbCases;
	}

	/**
	 * @return etat de la route (mont�e,descente,plat)
	 */
	public int etat() {
		return this.pente;
	}

	/**
	 * @return boolean � vrai si la tuile est en ligne droite
	 */
	public boolean estEnLigneDroite() {
		return (this.nomTuile == "D" || this.nomTuile == "A" || this.nomTuile == "LD");
	}

	/**
	 * @return boolean � vrai si la tuile est la tuile de d�part
	 */
	public boolean estDepart() {
		return this.nomTuile == "D";
	}

	/**
	 * @return boolean � vrai si la tuile est la tuile d'arriv�e
	 */
	public boolean estArrivee() {
		return this.nomTuile == "A";
	}

	/**
	 * @return boolean � vrai si la tuile est en ligne droite normale
	 */
	public boolean estLigneDroiteNormale() {
		return this.nomTuile == "LD";
	}

	/**
	 * @return boolean � vrai si la tuile est un virage l�ger
	 */
	public boolean estLeger() {
		return this.nomTuile == "VL";
	}

	/**
	 * @return boolean � vrai si la tuile est un virage serr�
	 */
	public boolean estSerrer() {
		return this.nomTuile == "VS";
	}

	/**
	 * @return boolean � vrai si la tuile est une mont�e
	 */
	public boolean estMontee() {
		return this.pente == 1;
	}

	/**
	 * @return boolean � vrai si la tuile est une descente
	 */
	public boolean estDescente() {
		return this.pente == 2;
	}
}
